import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import NotFound from "./components/NotFound";
import App from "./layouts/App";
import './assets/css/bootstrap4/bootstrap.min.css';
import './assets/plugin/font-awesome-4.7.0/css/font-awesome.min.css';
import './assets/css/main_styles.css';
import './assets/css/responsive.css';

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route exact path="/" component={App} />
      <Route component={NotFound} />
    </Switch>
  </Router>,
  document.getElementById("root")
);
