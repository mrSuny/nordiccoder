import App from './layouts/App';

const indexRoutes = [
    { path: "/", component: App },
    { path: "/productDetail/:productId", component: App }
];

export default indexRoutes;