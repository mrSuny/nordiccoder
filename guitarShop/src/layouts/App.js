import React, { Component } from "react";
import Header from "../components/Header/Header";
import HeroBanner from "../components/HeroBanner/HeroBanner";
import Banner from "../components/Banner/Banner";
import NewArrivals from "../components/NewArrivals/NewArrivals";
import Benefit from "../components/Benefit/Benefit";
import DealOfWeek from "../components/DealOfWeek/DealOfWeek";
import Products from "../data/Products"
import Footer from "../components/Footer/Footer";

class App extends Component {
  state = {
    Products: Products,
    orders: {}
  }

  addOrder = (key) => {
    console.log(`Ok roi nha ${key}`);

    //1. copy mảng oders ra 1 cai
    const orders = {...this.state.orders}

    //2. cộng order vào để update orders
    orders[key] = orders[key] + 1 || 1;

    //3. update cái state order lại để render ra 
    this.setState({
      orders
    })
  }
  
  render() {
    return (
      <div className="App">
        <Header orders={this.state.orders}/>

        <HeroBanner />

        <Banner />

        <NewArrivals loadProducts={this.state.Products} addOrder={this.addOrder} />

        <DealOfWeek />

        <Benefit />

        <Footer />
      </div>
    );
  }
}

export default App;
