import React from "react";
import {formatPrice} from "../../helpers/helpers";

class Product extends React.Component {
  handleClick = (event) => {
    event.preventDefault();
    this.props.addOrder(this.props.index);
  }
  render() {
    const { name, image, price, previousPrice } = this.props.details;
    console.log(this.props.details.name);

    return (
      <div className="product-item men">
        <div className="product discount product_filter">
        <div className="product_image">
            <img src={image} alt={name} />
        </div>
        <div className="favorite favorite_left"></div>
        <div className="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center"><span>-$20</span></div>
        <div className="product_info">
          <h6 className="product_name"><a href="single.html" title={name}>{name}</a></h6>
          <div className="product_price">{formatPrice(price)}<span>{
            previousPrice !== undefined ? formatPrice(previousPrice): ""
            }</span></div>
        </div>
        </div>
        <div className="red_button add_to_cart_button"><a href="#" onClick={this.handleClick}>add to cart</a></div>
      </div>
    )
  }
}

export default Product;