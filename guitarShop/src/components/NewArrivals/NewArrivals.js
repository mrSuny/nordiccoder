import React from "react";
import Product from "../Products/product";

class NewArrivals extends React.Component {
    render() {
        return (
          <div className="new_arrivals">
            <div className="container">
              <div className="row">
                <div className="col text-center">
                  <div className="section_title new_arrivals_title">
                    <h2>New Arrivals</h2>
                  </div>
                </div>
              </div>
              <div className="row align-items-center">
                <div className="col text-center">
                  <div className="new_arrivals_sorting">
                    <ul className="arrivals_grid_sorting clearfix button-group filters-button-group">
                      <li className="grid_sorting_button button d-flex flex-column justify-content-center align-items-center active is-checked" data-filter="*">all</li>
                      <li className="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".women">women's</li>
                      <li className="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".accessories">accessories</li>
                      <li className="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".men">men's</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col">
                  <div className="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
                    {Object.keys(this.props.loadProducts).map(key => (
                      <Product
                        key={key}
                        index={key}
                        details={this.props.loadProducts[key]}
                        addOrder={this.props.addOrder}
                      />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }
}

export default NewArrivals;