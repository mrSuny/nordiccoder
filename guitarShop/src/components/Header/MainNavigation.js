import React from "react";
import logo from "../../assets/images/logo.png";

class MainNavigation extends React.Component {
  render() {
    const orderId = Object.keys(this.props.orders);
    const total = orderId.reduce((total, key) => {
      const count = this.props.orders[key];
      return total + count;
    }, 0)

    return(
      <div className="main_nav_container">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-right">
              <div className="logo_container">
                <a href=""><img className="logo" src={logo} alt="" /></a>
              </div>
              <nav className="navbar">
                <ul className="navbar_menu">
                  <li><a href="">home</a></li>
                  <li><a href="categories.html">shop</a></li>
                  <li><a href="">promotion</a></li>
                  <li><a href="" target="blank">blog</a></li>
                  <li><a href="contact.html">contact</a></li>
                </ul>
                <ul className="navbar_user">
                  <li><a href=""><i className="fa fa-search" aria-hidden="true"></i></a></li>
                  <li><a href=""><i className="fa fa-user" aria-hidden="true"></i></a></li>
                  <li className="checkout">
                      <a href="">
                          <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                          {total > 0 ? <span id="checkout_items" className="checkout_items">{total}</span> : ""}
                          
                      </a>
                  </li>
                </ul>
                <div className="hamburger_container">
                  <i className="fa fa-bars" aria-hidden="true"></i>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MainNavigation;