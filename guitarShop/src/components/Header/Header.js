import React from "react";
// import PropTypes from "prop-types";

import TopNavigation from "./TopNavigation";
import MainNavigation from "./MainNavigation";

class Header extends React.Component {
  render() {
    return(
      <header className="header trans_300">

        {/* Top Navigation */}
        <TopNavigation />

        {/* Main Navigation */}
        <MainNavigation orders={this.props.orders} />

      </header>
    )
  }
}

export default Header;