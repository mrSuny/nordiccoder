import React from "react";
import bannerImage01 from "../../assets/images/guitarTaylor01.jpg";
import bannerImage02 from "../../assets/images/guitarTaylor02.jpg";
import bannerImage03 from "../../assets/images/guitarTaylor01.jpg";

var styleImage = [
  {backgroundImage: `url(${bannerImage01})`},
  {backgroundImage: `url(${bannerImage02})`},
  {backgroundImage: `url(${bannerImage03})`},
]

class Banner extends React.Component {
  render() {
    console.log(styleImage);

    return (
      <div className="banner">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="banner_item align-items-center" style={styleImage[0]}>
                <div className="banner_category">
                  <a href="categories.html">Acoustic Guitars</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="banner_item align-items-center" style={styleImage[1]}>
                <div className="banner_category">
                  <a href="categories.html">Archtop Guitars</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="banner_item align-items-center" style={styleImage[2]}>
                <div className="banner_category">
                  <a href = "categories.html"> Electric Guitars </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Banner;