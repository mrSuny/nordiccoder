// This is just some sample data so you don't have to think of your own!
const Products = {
    product1: {
        name: "Fujifilm X100T 16 MP Digital Camera (Silver)",
        image: "images/product_1.png",
        price: 1724,
        previousPrice: 2000
    },
    product2: {
        name: "Samsung CF591 Series Curved 27-Inch FHD Monitor",
        image: "images/product_2.png",
        price: 3000
    },
    product3: {
        name: "Blue Yeti USB Microphone Blackout Edition",
        image: "images/product_3.png",
        price: 45000,
        previousPrice: 48000
    },
    product4: {
        name: "Blue Yeti USB Microphone Blackout Edition",
        image: "images/product_4.png",
        price: 45000,
        previousPrice: 48000
    },
    product5: {
        name: "Blue Yeti USB Microphone Blackout Edition",
        image: "images/product_5.png",
        price: 45000,
        previousPrice: 48000
    }
};

export default Products;
